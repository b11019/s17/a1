/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
		function userId(){
			let userFirstName = prompt("Enter your First Name");
			let userLastName = prompt("Enter your Last Name");
			let userAge = prompt("Enter your Age");
			let userAddress = prompt("Enter your Complete Address");


			console.log(userFirstName + userLastName);
			console.log(userAge);
			console.log(userAddress);
		};
		userId();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	 function secondFunction(){
	 	let function2 = ["The Script", "Orange and Lemon", "Ben&Ben", "Metallica" , "Imagine Dragons"]
	 	console.log(function2);
	 };
	 secondFunction();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here: 
		function favMovies(){
			let mov1 = "The Originals"
			let rating1 = "IMDb rating 8.3/10"
			let mov2 = "The Legacies"
			let rating2 = "IMDb rating 7.3/10"
			let mov3 = "The Vampire Diaries"
			let rating3 = "IMDb rating 7.7/10"
			let mov4 = "Pirates of the Caribbean: The Curse of the Black Pearl"
			let rating4 = "IMDb rating 8.0/10"
			let mov5 = "Sherlock Holmes"
			let rating5 = "7.6/10"

			console.log("My Favorite Movies / TV Series:");
			console.log(mov1, rating1);
			console.log(mov2, rating2);
			console.log(mov3, rating3);
			console.log(mov4, rating4);
			console.log(mov5, rating5);
		};
		favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers()
	/*printFriends() = function printUsers()*/
	{
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	} ;


	printUsers();

/*console.log(friend1);
console.log(friend2);*/